<?php
/**
 * https://developer.wordpress.org/reference/functions/register_post_type/
 */

/**
 * CPT für Produktion
 * - gallery
 * - Kontext bezogenes Kontakt Formular Button
 */
add_action( 'init', 'bkw3s_add_cpt_gas_block_manufac' );
function bkw3s_add_cpt_gas_block_manufac() {
	$labels = [
		'name'               => _x( 'GAS-Block-Manufac', 'post type general name' ),
		'singular_name'      => _x( 'GAS-Block-Manufac', 'post type singular name' ),
		'add_new'            => _x( 'Hinzufügen', 'GAS-Block-Manufac' ),
		'add_new_item'       => __( 'Neuen GAS-Block-Manufac hinzufügen' ),
		'edit_item'          => __( 'GAS-Block-Manufac bearbeiten' ),
		'new_item'           => __( 'Neuen GAS-Block-Manufac' ),
		'view_item'          => __( 'GAS-Block-Manufac ansehen' ),
		'search_items'       => __( 'Nach GAS-Block-Manufac suchen' ),
		'not_found'          => __( 'Keine GAS-Block-Manufac gefunden' ),
		'not_found_in_trash' => __( 'Keine GAS-Block-Manufac im Papierkorb' ),
		'parent_item_colon'  => ''
	];

	$supports = [
		'title',
		'editor',
		'page-attributes',
		'custom-fields',
	];

	/**
	 * https://developer.wordpress.org/reference/functions/register_post_type/#parameters
	 *
	 */
	$args = [
		'labels'              => $labels,
		'description'         => 'Zeigt einen Text mit einer Galerie welche ein Vorschaubild hat.',
		'rewrite'             => [ 'slug' => 'gas-manufac/%gas_category_manufac%', 'with_front' => false ], // (bool|array)
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'query_var'           => true,
		'hierarchical'        => false,
		'has_archive'         => 'gas-manufac', // (bool|string) to display all posts from one post-type
		'menu_position'       => 5,
		'supports'            => $supports,
//		'taxonomies'         => [ 'category' ],
		'menu_icon'           => 'dashicons-format-aside',
	];
	register_post_type( 'gas_block_manufac', $args );
}

/**
 * https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
add_action( 'init', 'bkw3s_gas_block_manufac_taxonomy' );
function bkw3s_gas_block_manufac_taxonomy() {
	register_taxonomy(
		'gas_category_manufac',
		'gas_block_manufac',
		[
			'label'        => __( 'Genre' ),
			'rewrite'      => [ 'slug' => 'gas-manufac' ],
			'hierarchical' => true,
			'with_front'   => false,
		]
	);
}

if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'bkw3s_gas_block_manufac_tax_set_posttype' );
}
function bkw3s_gas_block_manufac_tax_set_posttype( $wp_query ) {

	if ( $wp_query->get( 'gas_category_manufac' ) ) {
		$wp_query->set( 'post_type', 'gas_block_manufac' );
	}
}

/**
 * Meta Box Description / HowTo
 */
add_action( 'admin_init', 'bkw3s_cpt_gas_block_manufac_metaboxs' );
function bkw3s_cpt_gas_block_manufac_metaboxs() {
	add_meta_box( 'meta-label', 'How to use', 'bkw3s_cpt_gas_block_manufac_metabox_howto', 'gas_block_manufac', 'normal', 'low' );
}

function bkw3s_cpt_gas_block_manufac_metabox_howto() {
	global $post;
	echo 'Use the following Shortcode to use this Custom-Post-Type GAS-Manufac-Block:
<br><strong>[bkw3s_block_manufac id="' . $post->ID . '"]</strong>
<br>...where id is the post ID.';
}

/**
 * Shortcode Display
 * Display one GAS Part Block
 */
function bkw3s_cpt_gas_block_manufac_display( $atts ) {
	// Post ID Comes from Shortcode
	if ( empty( $atts['id'] ) ) {
		return;
	}
	the_gas_block_manufac( $atts['id'] );
}

/**
 * For using this Contact Intermediary Form in content pages.
 * @return string
 */
add_shortcode( 'bkw3s_block_manufac', 'bkw3s_cpt_shortcode_manufac' );
function bkw3s_cpt_shortcode_manufac( $atts ) {
	ob_start();
	bkw3s_cpt_gas_block_manufac_display( $atts );

	return ob_get_clean();
}

/**
 * Function to use it in templates.
 *
 * @param int $postId
 *
 * @return string
 */
function the_gas_block_manufac( $postId = 0 ) {
	if ( empty( $postId ) ) {
		return;
	}
	/** @var WP_Post $post */
	$post = get_post( $postId );
	if ( ! $post instanceof WP_Post ) {
		return;
	}
	$fields = get_fields( $postId );
	if ( empty( $fields['part_blocklist_image'] ) ) {
		return;
	}
	$gallery = [];
	foreach ( $fields as $key => $field ) {
		if ( empty( $field ) || $key == 'part_blocklist_image' ) {
			continue;
		}
		$gallery[] = $field;
	}
	include BKW3S_GAS_VIEW_BASEPATH . '/cpt/bkw3s-block-manufac.php';
}