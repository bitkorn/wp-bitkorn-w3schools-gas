<?php

$f_h_bg_color = '#444444';
$f_h_text_color = '#dfddde';
$c_bg_color = '#888B91';
$c_text_color = '#0d0d0d';

add_action( 'customize_register', 'bkw3s_theme_customize_register' );
function bkw3s_theme_customize_register( $wp_customize ) {
    global $f_h_bg_color, $f_h_text_color, $c_bg_color, $c_text_color;

    /*
     * footer & header
     */
    $wp_customize->add_setting( 'footer_header_bg_color', array(
        'default'   => $f_h_bg_color,
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_header_bg_color', array(
        'section' => 'colors',
        'label'   => esc_html__( 'Footer & Header background color', 'theme' ),
    ) ) );

    $wp_customize->add_setting( 'footer_header_text_color', array(
        'default'   => $f_h_text_color,
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_header_text_color', array(
        'section' => 'colors',
        'label'   => esc_html__( 'Footer & Header Text color', 'theme' ),
    ) ) );

    /*
     * content
     */
    $wp_customize->add_setting( 'content_bg_color', array(
        'default'   => $c_bg_color,
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_bg_color', array(
        'section' => 'colors',
        'label'   => esc_html__( 'Content background color', 'theme' ),
    ) ) );

    $wp_customize->add_setting( 'content_text_color', array(
        'default'   => $c_text_color,
        'transport' => 'refresh',
        'sanitize_callback' => 'sanitize_hex_color',
    ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_text_color', array(
        'section' => 'colors',
        'label'   => esc_html__( 'Content Text color', 'theme' ),
    ) ) );
}

function bkw3s_theme_get_customizer_css() {
    global $f_h_bg_color, $f_h_text_color, $c_bg_color, $c_text_color;
    ob_start();

    $f_h_bg_c = get_theme_mod( 'footer_header_bg_color', $f_h_bg_color );
    $f_h_text_c = get_theme_mod( 'footer_header_text_color', $f_h_text_color);
    ?>
    header.page-header, .page-header-topping, footer.page-footer {
        background-color: <?php echo $f_h_bg_c; ?>;
        color: <?php echo $f_h_text_c; ?>;
    }
    <?php

    $c_bg_c = get_theme_mod( 'content_bg_color', $c_bg_color );
    $c_text_c = get_theme_mod( 'content_text_color', $c_text_color );
    ?>
    html, body {
        background-color: <?= $c_bg_c ?>;
    }
    #content {
        color: <?= $c_text_c ?>;
    }
    <?php

    $css = ob_get_clean();
    return $css;
}
function bkw3s_theme_enqueue_styles() {
    wp_enqueue_style( 'theme-styles', get_stylesheet_uri() ); // This is where you enqueue your theme's main stylesheet
    $custom_css = bkw3s_theme_get_customizer_css();
    wp_add_inline_style( 'theme-styles', $custom_css );
}

add_action( 'wp_enqueue_scripts', 'bkw3s_theme_enqueue_styles' );
