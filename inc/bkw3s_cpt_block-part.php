<?php
/**
 * https://developer.wordpress.org/reference/functions/register_post_type/
 */

/**
 * CPT für Produkte/Parts
 * - gallery
 * - Kontext bezogenes Kontakt Formular Button
 */
function bkw3s_add_cpt_gas_block_part() {
	$labels = [
		'name'               => _x( 'GAS-Block-Part', 'post type general name' ),
		'singular_name'      => _x( 'GAS-Block-Part', 'post type singular name' ),
		'add_new'            => _x( 'Hinzufügen', 'GAS-Block-Part' ),
		'add_new_item'       => __( 'Neuen GAS-Block-Part hinzufügen' ),
		'edit_item'          => __( 'GAS-Block-Part bearbeiten' ),
		'new_item'           => __( 'Neuen GAS-Block-Part' ),
		'view_item'          => __( 'GAS-Block-Part ansehen' ),
		'search_items'       => __( 'Nach GAS-Block-Parts suchen' ),
		'not_found'          => __( 'Keine GAS-Block-Parts gefunden' ),
		'not_found_in_trash' => __( 'Keine GAS-Block-Parts im Papierkorb' ),
		'parent_item_colon'  => ''
	];

	$supports = [
		'title',
		'editor',
		'page-attributes',
		'custom-fields',
//		'author',
//		'comments',
	];

	/**
	 * https://developer.wordpress.org/reference/functions/register_post_type/#parameters
	 *
	 */
	$args = [
		'labels'              => $labels,
		'description'         => 'Zeigt einen Text mit einer Galerie welche ein Vorschaubild hat.',
		'rewrite'             => [ 'slug' => 'gas-part/%gas_category_part%', 'with_front' => false ], // (bool|array)
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'query_var'           => true,
		'hierarchical'        => false,
		'has_archive'         => 'gas-part', // (bool|string) to display all posts from one post-type
		'menu_position'       => 4,
		'supports'            => $supports,
//		'taxonomies'         => [ 'category' ],
		'menu_icon'           => 'dashicons-format-aside',
	];
	register_post_type( 'gas_block_part', $args );
}

add_action( 'init', 'bkw3s_add_cpt_gas_block_part' );

/**
 * https://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function bkw3s_gas_block_part_taxonomy() {
	register_taxonomy(
		'gas_category_part',
		'gas_block_part',
		[
			'label'        => __( 'Genre' ),
			'rewrite'      => [ 'slug' => 'gas-part' ],
			'hierarchical' => true,
			'with_front'   => false,
		]
	);
}

add_action( 'init', 'bkw3s_gas_block_part_taxonomy' );

function bkw3s_gas_block_part_tax_set_posttype( $wp_query ) {

	if ( $wp_query->get( 'gas_category_part' ) ) {
		$wp_query->set( 'post_type', 'gas_block_part' );
	}
}

if ( ! is_admin() ) {
	add_action( 'pre_get_posts', 'bkw3s_gas_block_part_tax_set_posttype' );
}

function bkw3s_cpt_gas_block_part_metaboxs() {
	add_meta_box( 'meta-label', 'How to use', 'bkw3s_cpt_gas_block_part_metabox_howto', 'gas_block_part', 'normal', 'low' );
}

add_action( 'admin_init', 'bkw3s_cpt_gas_block_part_metaboxs' );
function bkw3s_cpt_gas_block_part_metabox_howto() {
	global $post;
	echo 'Use the following Shortcode to use this Custom-Post-Type GAS-Part-Block:
<br><strong>[bkw3s_block_part id="' . $post->ID . '"]</strong>
<br>...where id is the post ID.';
}

/**
 * Shortcode Display
 * Display one GAS Part Block
 */
function bkw3s_cpt_gas_block_part_display( $atts ) {
	// Post ID Comes from Shortcode
	if ( empty( $atts['id'] ) ) {
		return;
	}
	the_gas_block_part( $atts['id'] );
}

/**
 * For using this Contact Intermediary Form in content pages.
 * @return string
 */
function cpt_shortcode( $atts ) {
	ob_start();
	bkw3s_cpt_gas_block_part_display( $atts );

	return ob_get_clean();
}

add_shortcode( 'bkw3s_block_part', 'cpt_shortcode' );

/**
 * Function to use it in templates.
 *
 * @param int $postId
 *
 * @return string
 */
function the_gas_block_part( $postId = 0 ) {
	if ( empty( $postId ) ) {
		return;
	}
	/** @var WP_Post $post */
	$post = get_post( $postId );
	if ( ! $post instanceof WP_Post ) {
		return;
	}
	/**
	 * selector comes from inc/bkw3s_cpt_block-part.php (key | name)
	 * ...both are working :)
	 */
//	$image = get_field( 'field_5ea69018798f3', $post->ID );
//	$image = get_field( 'part_blocklist_image', $post->ID );
	$fields = get_fields( $postId );
	if ( empty( $fields['part_blocklist_image'] ) ) {
		return;
	}
	$gallery = [];
	foreach ( $fields as $key => $field ) {
		if ( empty( $field ) || $key == 'part_blocklist_image' ) {
			continue;
		}
		$gallery[] = $field;
	}
	include BKW3S_GAS_VIEW_BASEPATH . '/cpt/bkw3s-block-part.php';
}