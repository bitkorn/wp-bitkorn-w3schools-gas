# Install
## required WP plugins
- [plugins/advanced-custom-fields](https://wordpress.org/plugins/advanced-custom-fields/)
- [plugins/classic-editor](https://wordpress.org/plugins/classic-editor/)
- [plugins/polylang](https://de.wordpress.org/plugins/polylang/)
  - das kommt wohl erst spaeter
- [contact-form-7](https://wordpress.org/plugins/contact-form-7/)
  - [contact-form-7-dynamic-text-extension](https://wordpress.org/plugins/contact-form-7-dynamic-text-extension/)
- [WP Media Category Management](https://wordpress.org/plugins/wp-media-category-management/)

Zusammenarbeit mit WooCommerce & Polylang (Polylang for WooCommerce):
- [polylang.pro/downloads/polylang-for-woocommerce](https://polylang.pro/downloads/polylang-for-woocommerce/)
  - ...muss man sehen ob wir es brauchen ...ob wir WooCommerce nutzen.

## required static content with permalink slug
Statischer Inhalt & Permalinks ([permalink] => Content name)
 - [impressum] => Impressum
 - [datenschutzerklaerung] => Datenschutzerklaerung
 
### Contact Form 7 Dynamic Text Extension
1. Require a CF7 form with custom field named `product_details` & GET key `product-details`.
2. put shortcode from 1. into a WP page with SEO URL `/contact-product`.

Example request URL for the CF7-product_details:
[gomolzigde.lan/contact-product/?product-details=...](http://gomolzigde.lan/contact-product/?product-details=lorem%20ipsum%20dolor%0Aund%20nach%20einem%20newline)

Code for the "CF Product" CF7:
```html
<label> Your Name (required)
    [text* your-name class:w3-input] </label>

<label> Your Email (required)
    [email* your-email class:w3-input] </label>

<label> Product Details
[dynamictext product_details class:w3-input readonly "CF7_GET key='product-details'"] </label>

<label> Subject
    [text your-subject class:w3-input] </label>

<label> Your Message
    [textarea your-message class:w3-input] </label>

[submit "Send"]
```

## CPT
taxonomy categories:
- aircraft-components