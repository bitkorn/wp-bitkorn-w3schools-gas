/* 
 * 
 */

let checkAccept = function () {
    if(localStorage.getItem('privacy_accept') !== 'true') {
        document.getElementById('privacy-banner').className = 'w3-row';
    }
}

$(document).ready(function () {

    checkAccept();

    document.getElementById('privacy-accept-button').onclick = function () {
        localStorage.setItem('privacy_accept', 'true');
        document.getElementById('privacy-banner').className = 'w3-hide';
    };

    document.addEventListener('click', function (event) {
        if(event.target.tagName && event.target.tagName !== 'BUTTON' && event.target.tagName !== 'button') {
            $('.w3-dropdown-content').removeClass('w3-show');
        }
    });
});

function overlayClick() {
    $('.w3-dropdown-content').removeClass('w3-show');
    $('#overlay_menu').removeClass('w3-show');
}
function dropdownClick(dropdownId) {
    const el = $('#' + dropdownId);
    const overlay = $('#overlay_menu');
    const ddCont = $('.w3-dropdown-content');
    if(!el.hasClass('w3-show')) {
        // overlay.addClass('w3-show');
        ddCont.removeClass('w3-show');
        el.addClass('w3-show');
        return;
    }
    // overlay.removeClass('w3-show');
    ddCont.removeClass('w3-show');
}

function adjustJumbo(id) {
    const domRect = document.getElementById('nav-top').getBoundingClientRect();
    console.log('nav left: ' + domRect.left);
    document.getElementById(id).style.left = domRect.left + 'px';
    document.getElementById(id).style.width = domRect.width + 'px';
}
