<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 */
get_header();
global $wp_query, $logger;
$postTypes = get_query_var( 'post_type', [] );
if ( ! is_array( $postTypes ) ) {
	$postTypes = [];
}
//$logger->info(print_r($postTypes, true));
?>
    <div>
        <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" class="w3-row" style="width: 400px">
            <div class="w3-row">
                <div class="w3-col" style="width: 80%">
                    <input type="text" name="s" id="s" class="w3-input" value="<?php is_search() ? the_search_query() : null ?>">
                </div>
                <div class="w3-col" style="width: 20%">
                    <button type="submit" id="searchsubmit" class="w3-button gas-brand gas-search-button"><i class="fas fa-search"></i></button>
                </div>
            </div>

            <div class="w3-row">
                <div class="w3-col l6 m6 s6">
                    <label>
                        <input type="checkbox" name="post_type[]" value="gas_block_part" <?php if ( in_array( 'gas_block_part', $postTypes ) ) {
							echo 'checked="checked"';
						} ?> class="w3-check"> GAS Parts</label>
                </div>

                <div class="w3-col l6 m6 s6">
                    <label>
                        <input type="checkbox" name="post_type[]" value="gas_block_factory" <?php if ( in_array( 'gas_block_factory', $postTypes ) ) {
							echo 'checked="checked"';
						} ?> class="w3-check"> GAS Herstellung</label>
                </div>
            </div>
    </div>
    </form>
    <br>

<?php if ( have_posts() ) : ?>
    <header class="">
        <h2 class="gas-h">
			<?php printf( esc_html__( 'Search Results for: %s', 'wp-bitkorn-w3schools-gas' ), '<span>' . get_search_query() . '</span>' ); ?>
        </h2>
    </header><!-- .page-header -->

	<?php
	/* Start the Loop */
	while ( have_posts() ) : the_post();

		/**
		 * Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called content-search.php and that will be used instead.
		 */
		get_template_part( 'view/content', 'search' );

	endwhile;

	the_posts_navigation();

else :

	get_template_part( 'view/content', 'none' );
endif;
?>

    </div>

<?php
get_footer();
