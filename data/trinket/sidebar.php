<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<div class="w3-col l3 m3 w3-right w3-right-align" style="padding-left: 10px;">
    <div class="">
        <?php wp_nav_menu(array('theme_location' => 'site-menu-right')) ?>
        <?php if (is_active_sidebar('w3bk-sidebar-right')) : ?>
            <ul class="w3-ul">
                <?php dynamic_sidebar('w3bk-sidebar-right'); ?>
            </ul>
        <?php else : ?>
            <!-- Time to add some widgets! -->
        <?php endif; ?>
    </div>
</div>