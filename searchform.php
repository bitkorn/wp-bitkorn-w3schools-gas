<form role="search" method="get" id="searchform" class="w3-row" action="/" style="max-width: 300px">
<!--    <input type="hidden" name="post_type" value="gas_block_part"/>-->
    <div class="w3-col" style="width: 80%">
        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="search..." class="w3-input">
    </div>
    <div class="w3-col" style="width: 20%">
<!--        <input type="submit" id="searchsubmit" value="--><?//= __('Search', 'wp-bitkorn-w3schools-gas') ?><!--" class="w3-button gas-brand">-->
        <button type="submit" id="searchsubmit" class="w3-button gas-brand gas-search-button"><i class="fas fa-search"></i></button>
    </div>
</form>
