<?php
/**
 * Shortcode & regular display template
 *
 * shortcode display through bkw3s_cpt_gas_block_part_display()
 * and display through the_gas_block_part()
 */
?>
<div>
    <div class="w3-row">
        <div class="w3-col" style="width: 210px">
            <a class="cpt-blockpart-lightbox2-image-link" href="<?= $fields['part_blocklist_image']['url'] ?>"
               data-lightbox="part_blocklist_image_<?= $post->ID ?>"
               data-title="<?= $fields['part_blocklist_image']['title'] ?>"
            >
                <img src="<?= $fields['part_blocklist_image']['sizes']['W3BK_crop_200x200'] ?>" alt="<?= $fields['part_blocklist_image']['description'] ?>">
            </a>
            <?php foreach($gallery as $img): ?>
                <a href="<?= $img['url'] ?>" data-lightbox="part_blocklist_image_<?= $post->ID ?>" data-title="<?= $img['title'] ?>"></a>
            <?php endforeach; ?>
        </div>
        <div class="w3-rest">
            <header class="w3-padding gas-brand-discreet w3-card"><?= $post->post_title ?></header>
			<?= $post->post_content ?>
            <br>
            <a href="/contact-product?product-details=<?php echo urlencode($post->ID . ' | ' . $post->post_title) ?>"><i class="far fa-envelope"></i></a>
        </div>
    </div>
</div>
<br>