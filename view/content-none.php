<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
?>

<section class="no-results not-found">
    <header class="">
        <h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'wp-bitkorn-w3schools-gas' ); ?></h1>
    </header><!-- .page-header -->

    <div class="page-content">
		<?php if ( is_search() ) : ?>
            <p>
				<?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wp-bitkorn-w3schools-gas' ); ?>
            </p>
		<?php else : ?>
            <p>
				<?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'wp-bitkorn-w3schools-gas' ); ?>
            </p>
			<?php
			get_search_form();
		endif;
		?>
    </div><!-- .page-content -->
</section><!-- .no-results -->
