msgid ""
msgstr ""
"Project-Id-Version: GAS\n"
"POT-Creation-Date: 2020-04-30 10:30+0100\n"
"PO-Revision-Date: 2020-04-30 10:30+0100\n"
"Last-Translator: Torsten Brieskorn <mail@t-brieskorn.de>\n"
"Language-Team: GAS <mail@gomolzig.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop\n"
"X-Poedit-Basepath: .\n"
"Language: Englisch\n"

msgid "Search Results for: %s"
msgstr "Such Ergebnisse für: %s"

msgid "Contact"
msgstr "Kontakt"

msgid "Search"
msgstr "Suche"

msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Zu den Suchbegriffen konnte nichts gefunden werden"

msgid "Nothing Found"
msgstr "Nichts gefunden"

msgid "Pages:"
msgstr "Seiten:"

msgid "Edit %s"
msgstr "Bearbeite %s"

msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr "Weiter lesen <span class=\"meta-nav\">&rarr;</span>"