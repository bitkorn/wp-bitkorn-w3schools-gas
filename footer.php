</section>
</div><!-- class="w3-col l8 m10" -->
<div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
</div><!-- #content .w3-container -->
<footer class="page-footer w3-padding w3-row gas-text-brand">
    <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
    <div class="w3-col l8 m10">
        <div class="w3-row">
            <div class="w3-col l4 m5">
                <header class="page-footer-col-header">&nbsp;</header>
                <a href="<?= get_permalink( get_page_by_path( 'impressum' ) ) ?>" class="w3-button">Impressum</a>
                <br>
                <a href="<?= get_permalink( get_page_by_path( 'datenschutzerklaerung' ) ) ?>" class="w3-button">Datenschutz</a>
                <br>
                <a href="<?= get_stylesheet_directory_uri() ?>/data/doc/agb.pdf" target="_blank" class="w3-button">
                    <i class="far fa-file-pdf"></i> General Terms & Conditions</a>
            </div>
            <div class="w3-col l4 m5">
                <header class="page-footer-col-header">&nbsp;</header>
                <div style="width: 300px; margin: 0 auto">
                    <label for="newsletter">Newsletter registration</label>
                    <input type="email" id="newsletter" name="newsletter_email" placeholder="your.name@example.com" class="w3-input">
                </div>
            </div>
            <div class="w3-col l4 m5 w3-right-align">
                <header class="page-footer-col-header">&nbsp;</header>
                <a href="https://www.facebook.com/Gomolzig-Aircraft-Services-GmbH-100791240011882" class="w3-button" target="_blank">
                    Facebook <i class="fab fa-facebook-square w3-xlarge color-facebook"></i>
                </a>
                <br>
                <a href="https://www.linkedin.com/company/gomolzig-aircraft-services-gmbh" class="w3-button" target="_blank">
                    LinkedIn <i class="fab fa-linkedin w3-xlarge color-linkedin"></i>
                </a>
                <br>
            </div>
        </div>
    </div>
    <div class="w3-col l2 m1 w3-hide-small">&nbsp;</div>
</footer>
<div id="overlay_menu" onclick="overlayClick()"></div>
</div><!-- #page -->

<?php wp_footer(); ?>
<div id="privacy-banner" class="w3-row w3-hide">
    <div class="w3-col l11 m10">
        <p>
            Welcome! Do you accept the use of cookies for a better website functionality?
            <a href="<?= get_permalink( get_page_by_path( 'datenschutzerklaerung' ) ) ?>">Click here to read more about privacy.</a>
        </p>
    </div>
    <div class="w3-col l1 m2">
        <p>
            <button class="w3-button w3-blue-grey" id="privacy-accept-button">accept</button>
        </p>
    </div>
</div>

</body>
</html>