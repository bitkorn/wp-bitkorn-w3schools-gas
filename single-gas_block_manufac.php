<?php
get_header(); ?>

    <section id="primary">
        <main id="main" class="site-main">
			<?php
			if ( have_posts() ) : the_post();

//				get_template_part( 'view/content', get_post_format() );
				the_gas_block_part( get_the_ID() );
//			$custom_fields = acf_get_meta( get_the_ID() );
//			$logger->info($custom_fields);

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endif; // End of the loop.
			?>

        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();